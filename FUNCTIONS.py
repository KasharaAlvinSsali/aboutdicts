#FUNCTIONS
def my_name(fname):
    print(fname + "kash")
my_name("Alvin")
#passing more than one argument
def my_initials(name,email,school):
    print("My name is "+name)
    print("My email is " +email)
    print("My school is "+school)
my_initials("Kashara","alvinkashara20@gmial.com","Ntare")
# unknown number of arguments use * before the parameter
def my_fn(*kids):
    print("the oldest child is " + kids[2])
my_fn("Kash","Aluta","John")
#passing a list into a function
def my_taste(food):
    for x in food:
        print(x)
fruits=["apple","sushi","pottage"]
my_taste(fruits)
#return values
def add(x):
    return 4+x
print(add(3))
#pass for fns that are empty
def my_fn:
    pass
